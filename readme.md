<div align="center">
  <h1><code>tedbyron.com</code></h1>
  <p><strong>My website.</strong></p>
  <a href="https://github.com/tedbyron/tedbyron.com/actions/workflows/ci.yml">
    <img src="https://github.com/tedbyron/tedbyron.com/actions/workflows/ci.yml/badge.svg?branch=staging" alt="github actions status" />
  </a>
  <a href="https://app.netlify.com/sites/tedbyron/deploys">
    <img src="https://api.netlify.com/api/v1/badges/578043ec-2aa2-432a-8dea-d13155b076cf/deploy-status" alt="netlify deploy status" />
  </a>
</div>
