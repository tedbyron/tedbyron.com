import React from 'react'

import { Heading, Layout, Section } from '../components'

export default (): JSX.Element => (
  <Layout>
    <Section>
      <Heading as='h1' className='text-center'>Teddy Byron</Heading>
    </Section>
  </Layout>
)
